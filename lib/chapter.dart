import 'package:flutter/material.dart';

typedef ChapterBuilder = Widget Function(BuildContext context);

class Chapter {
  final String name;

  final ChapterBuilder builder;

  Chapter(this.name, this.builder);
}
import 'package:game_techniques_flame/light_and_shadows/main.dart';
import 'package:game_techniques_flame/raycasting/main.dart';
import 'package:game_techniques_flame/story.dart';
import 'package:flutter/material.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  runApp(MaterialApp(
    home: GameTechniquesShowCase(),
    debugShowCheckedModeBanner: false,
  ));
}

class GameTechniquesShowCase extends StatefulWidget {
  @override
  _GameTechniquesShowCaseState createState() => _GameTechniquesShowCaseState();
}

class _GameTechniquesShowCaseState extends State<GameTechniquesShowCase> {
  Story currentStory;

  List<Story> stories = [];

  @override
  void initState() {
    super.initState();

    stories.add(
      Story('Lights and Shadows')
        ..add('Simple boxes', (BuildContext context) {
          return LightsAndShadowGame.simpleBoxes(
            withFPS: false,
            lightStrength: 0.8,
            size: MediaQuery.of(context).size,
          ).widget;
        })
        ..add('Geometric shapes', (BuildContext context) {
          return LightsAndShadowGame.geometric(
            withFPS: false,
            lightStrength: 0.8,
            size: MediaQuery.of(context).size,
          ).widget;
        }),
    );

    stories.add(
      Story('Raycasting')
        ..add('Example', (BuildContext context) {
          return RaycastingGame(
            withFPS: false,
          ).widget;
        }),
    );
  }

  @override
  Widget build(BuildContext context) {
    if (currentStory == null) {
      setState(() {
        currentStory = stories[0];
      });
    }
    return Scaffold(
      appBar: AppBar(
        title: Text(currentStory.currentChapter.name),
      ),
      body: currentStory.currentChapter.builder(context),
      drawer: Drawer(
        child: ListView(
          children: <Widget>[
            ListTile(
              title: Text(
                'Game techniquess showcased using Flame',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            ListView.builder(
              primary: false,
              shrinkWrap: true,
              padding: EdgeInsets.zero,
              itemCount: stories.length,
              itemBuilder: (BuildContext context, int index) {
                var story = stories[index];
                return ExpansionTile(
                  initiallyExpanded: currentStory == story,
                  title: Text(story.name),
                  subtitle: Text('Chapters: ${story.chapters.length}'),
                  children: story.chapters.map<Widget>((chapter) {
                    return ListTile(
                      title: Text(
                        'Chapter ${story.chapters.indexOf(chapter) + 1}: ${chapter.name}',
                        style: TextStyle(
                          fontWeight: chapter == currentStory.currentChapter
                              ? FontWeight.bold
                              : FontWeight.normal,
                        ),
                      ),
                      onTap: () {
                        setState(() {
                          currentStory = story;
                          currentStory.currentChapter = chapter;
                        });
                        Navigator.of(context).pop();
                      },
                    );
                  }).toList(),
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}

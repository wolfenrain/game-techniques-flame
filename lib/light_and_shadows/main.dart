import 'dart:math';

import './light.dart';
import './polygon.dart';

import 'package:flame/game/base_game.dart';
import 'package:flame/gestures.dart';
import 'package:flame/position.dart';
import 'package:flame/text_config.dart';
import 'package:flutter/material.dart';

class LightsAndShadowGame extends BaseGame with MultiTouchDragDetector {
  List<Polygon> polygons = [];

  List<Light> lights = [];

  Polygon fieldOfView;

  Position lightPosition = Position(320, 180);

  @override
  Color backgroundColor() => const Color(0xFF999999);

  TextConfig config = TextConfig(fontSize: 16.0, color: Colors.lime);

  bool withFPS;

  double lightStrength;

  LightsAndShadowGame({this.withFPS, this.lightStrength})
      : assert(lightStrength != null && lightStrength <= 1);

  factory LightsAndShadowGame.simpleBoxes({
    bool withFPS,
    double lightStrength,
    Size size,
  }) {
    var game = LightsAndShadowGame(
      withFPS: withFPS,
      lightStrength: lightStrength,
    );

    var boxSize = 40.0;
    for (double x = 0; x < size.width / boxSize; x++) {
      game.addPolygon(Polygon.fromRect(
        Rect.fromLTWH(
          (boxSize * x + (boxSize * x)),
          100,
          boxSize / 2,
          boxSize / 2,
        ),
      ));

      game.addPolygon(Polygon.fromRect(
        Rect.fromLTWH(
          (boxSize * x + (boxSize * x)),
          size.height - 120,
          boxSize / 2,
          boxSize / 2,
        ),
      ));
    }

    game.addPolygon(Polygon.fromRect(
      Rect.fromLTWH(
        size.width / 2 - 50,
        size.height / 2 - 50,
        100,
        100,
      ),
    ));

    game.lights.add(Light(Position(300, 300), game.lightStrength));
    game.lights.add(Light(Position(100, 300), 0.8));

    return game;
  }

  factory LightsAndShadowGame.geometric({
    bool withFPS,
    double lightStrength,
    Size size,
  }) {
    var game = LightsAndShadowGame(
      withFPS: withFPS,
      lightStrength: lightStrength,
    );

    game.addPolygon(Polygon.sided(
        sides: 3,
        size: 40,
        rotation: Random().nextInt(360).toDouble(),
        position: Position(100, 100)));

    game.addPolygon(Polygon.sided(
        sides: 4,
        size: 40,
        rotation: Random().nextInt(360).toDouble(),
        position: Position(200, 200)));

    game.addPolygon(Polygon.sided(
        sides: 5,
        size: 40,
        rotation: Random().nextInt(360).toDouble(),
        position: Position(300, 300)));

    game.addPolygon(Polygon.sided(
        sides: 6,
        size: 40,
        rotation: Random().nextInt(360).toDouble(),
        position: Position(200, 400)));

    game.addPolygon(Polygon.sided(
        sides: 7,
        size: 40,
        rotation: Random().nextInt(360).toDouble(),
        position: Position(100, 500)));

    game.lights.add(Light(Position(100, 300), game.lightStrength));

    return game;
  }

  void addPolygon(Polygon polygon) {
    this.polygons.add(polygon);
    this.add(polygon);
  }

  @override
  bool recordFps() => withFPS;

  @override
  void render(Canvas canvas) {
    // We render all of our components first,
    // before applying the lighting and shadows effect.
    super.render(canvas);

    Rect rect = Offset.zero & size;

    // Saving the current layer.
    canvas.saveLayer(rect, Paint());

    canvas.drawColor(Colors.black.withAlpha(200), BlendMode.dstATop);

    // Drawing a full size rectangle that will act as our shadow.
    // canvas.drawRect(rect, Paint()..color = Colors.black.withAlpha(150));

    // Loop through each light and render the effect.
    for (var l in lights) {
      l.render(canvas, size, polygons);
    }

    // Restore the layer.
    canvas.restore();

    if (recordFps()) {
      config.render(
        canvas,
        'FPS: ${fps(60).toStringAsFixed(2)}',
        Position(0, 0),
      );
    }
  }

  @override
  void onReceiveDrag(DragEvent drag) {
    this.lights[0].position = Position.fromOffset(drag.initialPosition);
    drag.onUpdate = (DragUpdateDetails details) {
      this.lights[0].position = Position.fromOffset(details.localPosition);
    };
    super.onReceiveDrag(drag);
  }
}

import './segment.dart';

import 'package:flame/position.dart';

class EndPoint {
  bool beginsSegment = false;

  Segment segment;

  double angle = 0;

  Position position;

  EndPoint(double x, double y) : position = Position(x, y);

  @override
  bool operator ==(Object other) {
    if (other is EndPoint) {
      return position.x == other.position.x &&
          position.y == other.position.y &&
          beginsSegment == other.beginsSegment &&
          angle == other.angle;
    }
    return false;
  }

  @override
  int get hashCode =>
      beginsSegment.hashCode + segment.hashCode + angle.hashCode;

  @override
  String toString() {
    return "{ position: ${position.toString()}, angle: $angle }";
  }
}

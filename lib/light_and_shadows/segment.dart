import 'dart:ui';

import './end_point.dart';
import './polygon.dart';

import 'package:flame/position.dart';

class Segment {
  /// First [EndPoint] that makes up this [Segment].
  final EndPoint p1;

  /// Second [EndPoint] that makes up this [Segment].
  final EndPoint p2;

  double d = 0;

  List<EndPoint> get points {
    return [this.p1, this.p2];
  }

  Segment(Position v1, Position v2)
      : p1 = EndPoint(v1.x, v1.y),
        p2 = EndPoint(v2.x, v2.y) {
    p1.segment = this;
    p2.segment = this;
  }

  @override
  bool operator ==(Object other) {
    if (other is Segment) {
      return p1 == other.p1 && p2 == other.p2;
    }
    return false;
  }

  @override
  int get hashCode => p1.hashCode + p2.hashCode;

  @override
  String toString() {
    return "{ p1: $p1, p2: $p2 }";
  }

  /// Construct a list of segments from a rectangle.
  static List<Segment> fromRectangle(Rect rect) {
    return [
      Segment(
        Position.fromOffset(rect.topLeft),
        Position.fromOffset(rect.topRight),
      ),
      Segment(
        Position.fromOffset(rect.topLeft),
        Position.fromOffset(rect.bottomLeft),
      ),
      Segment(
        Position.fromOffset(rect.topRight),
        Position.fromOffset(rect.bottomRight),
      ),
      Segment(
        Position.fromOffset(rect.bottomLeft),
        Position.fromOffset(rect.bottomLeft),
      )
    ];
  }

  /// Construct a list of segments from a polygon.
  static List<Segment> fromPolygon(Position pos, Polygon polygon) {
    List<Segment> segments = [];
    List<double> points = [];

    polygon.points.forEach((point) => points.addAll([point.x, point.y]));

    if (points.length != 0) {
      segments.add(Segment(
        Position(points[0] + pos.x, points[1] + pos.y),
        Position(points[2] + pos.x, points[3] + pos.y),
      ));

      for (var i = 0; i < points.length; i += 2) {
        Position xPos;
        Position yPos;
        try {
          xPos = Position(points[i] + pos.x, points[i + 1] + pos.y);
          yPos = Position(points[i + 2] + pos.x, points[i + 3] + pos.y);
        } on RangeError catch (_) {
          xPos = Position(points[0] + pos.x, points[1] + pos.y);
          yPos = Position(points[i] + pos.x, points[i + 1] + pos.y);
        }

        segments.add(Segment(xPos, yPos));
      }
    }

    return segments;
  }
}

import 'dart:math';

import './end_point.dart';
import './polygon.dart';
import './segment.dart';

import 'package:flame/position.dart';

/// Computes a list of positions that represents which regions are
/// visibile from the origin point given a set of segments.
class VisibilityComputer {
  List<EndPoint> _endpoints = [];
  List<Segment> _segments = [];

  Position origin;

  double radius;

  VisibilityComputer(this.origin, this.radius) {
    _loadBoundaries();

    var steps = 45;
    for (var i = 0; i < steps; i++) {
      var angle1 = ((360 / steps) * i) * (pi / 180);
      var angle2 = ((360 / steps) * (i + 1)) * (pi / 180);
      var p1 = Position(origin.x + (radius * cos(angle1)), origin.y + (radius * sin(angle1)));
      var p2 = Position(origin.x + (radius * cos(angle2)), origin.y + (radius * sin(angle2)));
      _addSegment(p1, p2);
    }

  }

  /// Construct segments along the outside perimiter.
  void _loadBoundaries() {
    //Top
    _addSegment(Position(origin.x - radius, origin.y - radius),
        Position(origin.x + radius, origin.y - radius));

    //Bottom
    _addSegment(Position(origin.x - radius, origin.y + radius),
        Position(origin.x + radius, origin.y + radius));

    //Left
    _addSegment(Position(origin.x - radius, origin.y - radius),
        Position(origin.x - radius, origin.y + radius));

    //Right
    _addSegment(Position(origin.x + radius, origin.y - radius),
        Position(origin.x + radius, origin.y + radius));
    // addPolygon(Polygon.fromRect(Offset.zero & size));
  }

  /// Add a [Segment], where the first point shows up in the
  /// visualization but the second one does not. (Every [EndPoint] is
  /// part of two segments, but we want to only show them once.)
  void _addSegment(Position p1, Position p2) {
    Segment segment = Segment(p1, p2);

    _segments.add(segment);
    _endpoints.add(segment.p1);
    _endpoints.add(segment.p2);
  }

  void _updateSegments() {
    for (Segment segment in _segments) {
      // NOTE: future optimization: we could record the quadrant
      // and the y/x or x/y ratio, and sort by (quadrant,
      // ratio), instead of calling atan2. See
      // <https://github.com/mikolalysenko/compare-slope> for a
      // library that does this.

      segment.p1.angle = atan2(
          segment.p1.position.y - origin.y, segment.p1.position.x - origin.x);
      segment.p2.angle = atan2(
          segment.p2.position.y - origin.y, segment.p2.position.x - origin.x);

      // Map angle between -Pi and Pi
      var dAngle = segment.p2.angle - segment.p1.angle;
      if (dAngle <= -pi) {
        dAngle += pi * 2;
      }
      if (dAngle > pi) {
        dAngle -= pi * 2;
      }

      segment.p1.beginsSegment = dAngle > 0.0;
      segment.p2.beginsSegment = !segment.p1.beginsSegment;
    }
  }

  /// Calculates if segment a is in front of segment b.
  ///
  /// The implementation is not anti-symmetric:
  /// ```dart
  /// _segmentInFrontOf(a, b) != (!_segmentInFrontOf(b, a)
  /// ```
  ///
  /// For more information see:
  /// cases. See http://www.redblobgames.com/articles/visibility/segment-sorting.html
  bool _segmentInFrontOf(Segment a, Segment b, Position relativeTo) {
    // NOTE: we slightly shorten the segments so that
    // intersections of the endpoints (common) don't count as
    // intersections in this algorithm.

    bool a1 = leftOf(a.p2.position, a.p1.position,
        interpolate(b.p1.position, b.p2.position, 0.01));
    bool a2 = leftOf(a.p2.position, a.p1.position,
        interpolate(b.p2.position, b.p1.position, 0.01));
    bool a3 = leftOf(a.p2.position, a.p1.position, relativeTo);

    bool b1 = leftOf(b.p2.position, b.p1.position,
        interpolate(a.p1.position, a.p2.position, 0.01));
    bool b2 = leftOf(b.p2.position, b.p1.position,
        interpolate(a.p2.position, a.p1.position, 0.01));
    bool b3 = leftOf(b.p2.position, b.p1.position, relativeTo);

    // NOTE: this algorithm is probably worthy of a short article
    // but for now, draw it on paper to see how it works. Consider
    // the line A1-A2. If both B1 and B2 are on one side and
    // relativeTo is on the other side, then A is in between the
    // viewer and B. We can do the same with B1-B2: if A1 and A2
    // are on one side, and relativeTo is on the other side, then
    // B is in between the viewer and A.
    if (b1 == b2 && b2 != b3) return true;
    if (a1 == a2 && a2 == a3) return true;
    if (a1 == a2 && a2 != a3) return false;
    if (b1 == b2 && b2 == b3) return false;

    return false;
  }

  void _addTriangle(
      List<Position> triangles, double angle1, double angle2, Segment segment) {
    Position p1 = origin;
    Position p2 = Position(origin.x + cos(angle1), origin.y + sin(angle1));
    Position p3 = Position.empty();
    Position p4 = Position.empty();

    if (segment != null) {
      // Stop the triangle at the intersecting segment.
      p3.x = segment.p1.position.x;
      p3.y = segment.p1.position.y;
      p4.x = segment.p2.position.x;
      p4.y = segment.p2.position.y;
    } else {
      // Stop the triangle at a fixed distance.
      // This is not ideal, but it does the trick.
      p3.x = origin.x + cos(angle1) * radius * 2;
      p3.y = origin.y + sin(angle1) * radius * 2;
      p4.x = origin.x + cos(angle2) * radius * 2;
      p4.y = origin.y + sin(angle2) * radius * 2;
    }

    Position pBegin = lineLineIntersection(p3, p4, p1, p2);

    p2.x = origin.x + cos(angle2);
    p2.y = origin.y + sin(angle2);

    Position pEnd = lineLineIntersection(p3, p4, p1, p2);

    triangles.add(pBegin);
    triangles.add(pEnd);
  }

  void addPolygon(Polygon polygon) {
    var segments = Segment.fromPolygon(polygon.position, polygon);

    for (var s in segments) {
      _addSegment(s.p1.position, s.p2.position);
    }
  }

  List<Position> compute() {
    List<Position> output = [];
    List<Segment> open = [];

    _updateSegments();

    _endpoints.sort((pointA, pointB) {
      if (pointA.angle > pointB.angle) return 1;
      if (pointA.angle < pointB.angle) return -1;
      if (!pointA.beginsSegment && pointB.beginsSegment) return 1;
      if (pointA.beginsSegment && !pointB.beginsSegment) return -1;
      return 0;
    });

    double currentAngle = 0;

    // At the beginning of the sweep we want to know which
    // segments are active. The simplest way to do this is to make
    // a pass collecting the segments, and make another pass to
    // both collect and process them. However it would be more
    // efficient to go through all the segments, figure out which
    // ones intersect the initial sweep line, and then sort them.
    for (var pass = 0; pass < 2; pass++) {
      for (EndPoint p in _endpoints) {
        Segment currentOld = open.length == 0 ? null : open.first;

        if (p.beginsSegment) {
          var index = 0;
          // Insert into the right place in the list
          var node = open.length == 0 ? null : open[index];
          while (node != null && _segmentInFrontOf(p.segment, node, origin)) {
            index++;
            node = open.length == index ? null : open[index];
          }

          if (node == null) {
            open.add(p.segment);
          } else {
            open.insert(index, p.segment);
          }
        } else {
          open.remove(p.segment);
        }

        Segment currentNew;
        if (open.length != 0) {
          currentNew = open.first;
        }

        if (currentOld != currentNew) {
          if (pass == 1) {
            _addTriangle(output, currentAngle, p.angle, currentOld);
          }
          currentAngle = p.angle;
        }
      }
    }

    return output;
  }

  /// Computes the intersection point of the line p1-p2 with p3-p4.
  static Position lineLineIntersection(
      Position p1, Position p2, Position p3, Position p4) {
    // From http://paulbourke.net/geometry/lineline2d/
    var x = ((p4.x - p3.x) * (p1.y - p3.y) - (p4.y - p3.y) * (p1.x - p3.x));
    var y = ((p4.y - p3.y) * (p2.x - p1.x) - (p4.x - p3.x) * (p2.y - p1.y));
    var s = x / (y == 0 ? 1 : y);
    return Position(p1.x + s * (p2.x - p1.x), p1.y + s * (p2.y - p1.y));
  }

  /// Returns if the point is 'left' of the line p1-p2.
  static bool leftOf(Position p1, Position p2, Position point) {
    var cross =
        (p2.x - p1.x) * (point.y - p1.y) - (p2.y - p1.y) * (point.x - p1.x);

    return cross < 0;
  }

  /// Returns a slightly shortened version of the vector:
  /// ```
  /// p * (1 - f) + q * f
  /// ```
  static Position interpolate(Position p, Position q, double f) {
    return Position(p.x * (1.0 - f) + q.x * f, p.y * (1.0 - f) + q.y * f);
  }
}

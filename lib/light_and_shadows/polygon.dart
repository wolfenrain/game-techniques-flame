import 'dart:math';
import 'dart:ui';

import 'package:flame/components/component.dart';
import 'package:flame/position.dart';
import 'package:flutter/material.dart';

class Polygon extends Component {
  Position position;

  List<Position> points = [];

  bool visible = true;

  Path get path {
      var path = Path();
     path.addPolygon(
        points.map<Offset>((point) {
          return Position.fromPosition(point).add(position).toOffset();
        }).toList(),
        true,
      );
      return path;
  }

  /// Create a custom polygon shape.
  ///
  /// Supply a list of [Position] to create the shape.
  /// You can set the [position] of the polygon.
  ///
  /// ```dart
  /// Polygon([
  ///   Position(0, 0),
  ///   Position(100, 25),
  ///   Position(156, 120),
  ///   Position(90, 130),
  ///   Position(30, 200),
  ///   Position(0, 0)
  /// ]);
  /// ````
  Polygon(this.points, {this.position}) {
    if (position == null) {
      position = Position.empty();
    }
  }

  factory Polygon.fromRect(Rect rect, {
    double rotation = 0
  }) {
    return Polygon(
      [
        Position(0, 0).rotateDeg(rotation),
        Position(rect.width, 0).rotateDeg(rotation),
        Position(rect.width, rect.height).rotateDeg(rotation),
        Position(0, rect.height).rotateDeg(rotation),
      ],
      position: Position(rect.left, rect.top),
    );
  }

  /// Create a x-sided polygon.
  ///
  /// Create a polygon with as many [sides] as you want(minimal of 2).
  /// With the size or width and height you can manipulate its size.
  /// Set the [rotation](degrees) if you wish to rotate the Polygon.
  ///
  /// Creating a hexagon shaped polygon:
  /// ```dart
  /// Polygon.sided(
  ///   sides: 6,
  ///   size: 100
  /// );
  /// ```
  ///
  /// Creating a diamond shape polygon:
  /// ```dart
  /// Polygon.sided(
  ///   sides: 4,
  ///   width: 50,
  ///   height: 100,
  /// );
  /// ```
  ///
  /// The [rotation] is in degrees.
  factory Polygon.sided({
    int sides,
    int size,
    int width,
    int height,
    double rotation = 45,
    Position position,
  }) {
    assert(sides > 1);
    assert(size != null && width == null && height == null ||
        size == null && width != null && height != null);
    if (size != null) {
      width = size;
      height = size;
    }
    List<Position> positions = [];

    for (var side = 0; side <= sides; side++) {
      var pos = Position(
        width * cos(side * 2 * pi / sides),
        height * sin(side * 2 * pi / sides),
      );
      pos.rotate(rotation * (pi / 180));

      positions.add(pos);
    }
    return Polygon(positions, position: position);
  }

  bool contains(double x, double y) {
    return path.contains(Offset(x, y));
  }

  @override
  void render(Canvas canvas) {
    if (visible) {
      canvas.drawPath(
          path,
          Paint()
            ..color = Colors.red
            ..isAntiAlias = true);
    }
  }

  @override
  void update(double delta) {}
}

import 'dart:ui';

import './polygon.dart';
import './visibility_computer.dart';

import 'package:flame/position.dart';
import 'package:flutter/material.dart';

class Light {
  Position position;

  double radius;

  Light(this.position, this.radius) : assert(radius <= 1);

  void render(Canvas canvas, Size size, List<Polygon> polygons) {
    var visibility = VisibilityComputer(position, 100 * radius);

    for (var p in polygons) {
      visibility.addPolygon(p);
    }

    var align = size.center(Offset.zero);

    final Gradient gradient = RadialGradient(
      center: Alignment(
        -(align.dx - position.x) / align.dx,
        -(align.dy - position.y) / align.dy,
      ),
      radius: radius,
      colors: <Color>[
        Colors.transparent,
        Colors.black,
      ],
      stops: [
        0.0,
        1.0,
      ],
    );

    final Paint paint = new Paint()
      ..shader = gradient.createShader(Offset.zero & size)
      ..blendMode = BlendMode.dstIn;

    var encounters = visibility.compute();

    canvas.save();

    canvas.drawPath(
      Polygon(encounters).path,
      Paint()
        ..blendMode = BlendMode.clear
        ..maskFilter = MaskFilter.blur(
          BlurStyle.normal,
          5,
        ),
    );

    canvas.restore();
  }
}

import 'dart:math';

import './camera.dart';

import 'package:flame/game/base_game.dart';
import 'package:flame/gestures.dart';
import 'package:flame/position.dart';
import 'package:flame/text_config.dart';
import 'package:flutter/material.dart';

class RaycastingGame extends BaseGame with HorizontalDragDetector {
  Camera gameCamera;

  // Size of each cell.
  double _cellSize = 3;

  List<Color> _colorMap = [
    Colors.pink,
    Colors.green,
    Colors.amber,
    Colors.brown,
    Colors.blue
  ];

  List<List<int>> _map = [
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 4, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 4, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0],
    [0, 0, 0, 0, 0, 0, 2, 1, 2, 0, 0, 0, 2, 2, 0, 0, 0, 4, 0, 0],
    [0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 2, 2, 0, 0, 0, 1, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 4, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0],
    [3, 3, 3, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [3, 0, 0, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3, 3, 3, 3, 3],
    [3, 3, 3, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 1, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 4],
    [0, 0, 3, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 4],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 4],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 4, 4],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 4, 4]
  ];

  List<Map<String, dynamic>> _scene = [];

  List<Map<String, dynamic>> _lines = [];

  double sceneWidth;

  double sceneHeight;

  TextConfig config = TextConfig(fontSize: 16.0, color: Colors.white);

  bool withFPS;

  RaycastingGame({
    this.withFPS,
  }) {
    sceneWidth = _map[0].length * _cellSize;
    sceneHeight = _map.length * _cellSize;
    gameCamera = Camera(Position(2 * _cellSize, 2 * _cellSize));
  }

  @override
  bool recordFps() => withFPS;

  @override
  void render(Canvas canvas) {
    _drawMap(canvas);

    _drawCircle(
      canvas,
      gameCamera.position.x,
      gameCamera.position.y,
      _cellSize,
    );

    if (_scene.length != 0) {
      var w = size.width ~/ _scene.length;
      for (var i = 0; i < _scene.length; i++) {
        double h = _scene[i]['h'];

        var alpha = h.toInt();
        alpha = alpha > 255 ? 255 : alpha;

        var x = i + (i * w);
        var y = size.height ~/ 2;

        var paint = Paint()
          ..color = _colorMap[_scene[i]['val']].withAlpha(alpha);

        canvas.drawRect(
          Rect.fromLTWH(x.toDouble(), y.toDouble(), w.toDouble(), h),
          paint,
        );
      }
    }
    super.render(canvas);

    if (recordFps()) {
      config.render(
        canvas,
        fps(60).toStringAsFixed(2),
        Position(sceneWidth, 0),
      );
    }
  }

  @override
  void update(double t) {
    super.update(t);
    if (size == null) {
      return;
    }
    _castRays();
  }

  @override
  void onHorizontalDragUpdate(DragUpdateDetails details) {
    if (details.delta.dx < 0) {
      gameCamera.rotation += 0.01;
      gameCamera.angle += 0.01;
    }
    if (details.delta.dx > 0) {
      gameCamera.rotation -= 0.01;
      gameCamera.angle -= 0.01;
    }
    super.onHorizontalDragUpdate(details);
  }

  void _drawMap(Canvas canvas) {
    _lines.forEach((line) {
      canvas.drawLine(
        Offset(line['srcX'], line['srcY']),
        Offset(line['rayX'], line['rayY']),
        Paint()..color = Colors.white,
      );
    });
    for (var y = 0; y < _map.length; y++) {
      for (var x = 0; x < _map[y].length; x++) {
        // If the value of the map is part of the colorMap, than we draw it on the map.
        if (_map[y][x] > 0 && _map[y][x] < _colorMap.length) {
          var color = _colorMap[_map[y][x]];
          canvas.drawRect(
            Rect.fromLTWH(x * _cellSize, y * _cellSize, _cellSize, _cellSize),
            Paint()..color = color,
          );
        }
      }
    }
  }

  void _drawCircle(Canvas canvas, double x, double y, double r) {
    canvas.drawCircle(Offset(x, y), r, Paint()..color = Colors.orange);
  }

  // Cast ray, and when hit add it to the scene for rendering.
  void _castRay(double srcX, double srcY, double angle) {
    var rayX = srcX + cos(angle);
    var rayY = srcY + sin(angle);
    double dst = 0;

    var isHit = false;
    while (!isHit && dst < sceneWidth) {
      dst += 0.1;
      rayX = srcX + cos(angle) * dst;
      rayY = srcY + sin(angle) * dst;

      var row = rayY ~/ _cellSize;
      var col = rayX ~/ _cellSize;
      var a = gameCamera.angle - angle;
      var z = dst * cos(a);
      var h = size.height * 4 / z;

      // Check for world boundaries
      if (rayX > size.width - 4 ||
          rayX < 4 ||
          rayY < 4 ||
          rayY > size.height - 4) {
        isHit = true;
        _scene.add({'h': h, 'val': 4});
      } else {
        // Check for cell values, if it is either 0 or part of the colorMap, add it to the scene it.
        if (_map.length > row &&
            _map[row].length > col &&
            _map[row][col] > 0 &&
            _map[row][col] < _colorMap.length) {
          isHit = true;
          _scene.add({'h': h, 'val': _map[row][col]});
        }
      }
    }
    
    // Raycast visualizer for on the mini map.
    _lines.add({
      'srcX': srcX,
      'srcY': srcY,
      'rayX': rayX,
      'rayY': rayY,
    });
  }

  /// Cast rays for the whole [Camera.fieldOfView].
  void _castRays() {
    _scene = [];
    _lines = [];

    for (var x = -gameCamera.fieldOfView / 2;
        x < gameCamera.fieldOfView / 2;
        x += 0.5) {
      var rayAngle = gameCamera.angle + toRadian(x);
      _castRay(gameCamera.position.x, gameCamera.position.y, rayAngle);
    }
  }
}

import 'dart:math';
import 'dart:ui';

import 'package:flame/components/component.dart';
import 'package:flame/position.dart';

double toRadian(double d) {
  return d * pi / 180;
}

class Camera {
  /// Range of area you can see.
  int fieldOfView = 60;

  /// Position of the [Camera].
  Position position;

  /// Current rotation of the Camera.
  double rotation = 0;

  double angle = toRadian(45);

  Camera(this.position);
}

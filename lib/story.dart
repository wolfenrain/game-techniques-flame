import 'package:game_techniques_flame/chapter.dart';

class Story {
  final String name;

  final List<Chapter> chapters = [];

  Chapter currentChapter;

  Story(this.name);

  void add(String chapterName, ChapterBuilder builder) {
    var chapter = Chapter(chapterName, builder);
    chapters.add(chapter);

    if (currentChapter == null) {
      currentChapter = chapter;
    }
  }
}